<?php
class PersegiEmpat
{
    private float $panjang;
    private float $lebar;

    public function setPanjang($p)
    {
        $this->panjang = $p;
    }

    public function getPanjang()
    {
        return $this->panjang;
    }

    public function setLebar($l)
    {
        $this->lebar = $l;
    }

    public function getLebar()
    {
        return $this->lebar;
    }

    public function getLuas(): float
    {
        return $this->getPanjang() * $this->getLebar();
    }
}

class Kubus extends PersegiEmpat
{
    private float $tinggi;
    private float $volume;

    public function setTinggi($t)
    {
        $this->tinggi = $t;
    }

    public function getTinggi()
    {
        return $this->tinggi;
    }

    public function setVolume($p, $l, $t)
    {
        $this->setPanjang($p);
        $this->setLebar($l);
        $this->setTinggi($t);
        $this->volume = $this->getLuas() * $this->getTinggi();
    }

    public function getVolume()
    {
        return $this->volume;
    }
}

class Balok extends PersegiEmpat
{
    private float $tinggi;
    private float $volume;

    public function setTinggi($t)
    {
        $this->tinggi = $t;
    }

    public function getTinggi()
    {
        return $this->tinggi;
    }

    public function setVolume($p, $l, $t)
    {
        $this->setPanjang($p);
        $this->setLebar($l);
        $this->setTinggi($t);
        $this->volume = $this->getLuas() * $this->getTinggi();
    }

    public function getVolume()
    {
        return $this->volume;
    }
}

$kbs = new Kubus();
$kbs->setVolume(10, 10, 10);
echo "Volume Kubus : " . $kbs->getVolume();

echo "<br>";

$blk = new Balok();
$blk->setVolume(10, 5, 7);
echo "Volume Balok : " . $blk->getVolume();
